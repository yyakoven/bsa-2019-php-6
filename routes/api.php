<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products', 'API\ProductController@index');
Route::get('products/popular', 'API\ProductController@popular');
Route::get('products/cheap', 'API\ProductController@cheap');