<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction
{
    protected $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetAllProductsResponse
    {
        $allProducts = $this->repository->findAll();
        return new GetAllProductsResponse($allProducts);
    }
}