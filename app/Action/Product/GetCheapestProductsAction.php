<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    protected $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        $allProducts = $this->repository->findAll();
        $cheapest = collect($allProducts)->sortBy(function($product) {
            return($product->getPrice());
        })->take(3)->values()->toArray();
        return new GetCheapestProductsResponse($cheapest);
    }
}