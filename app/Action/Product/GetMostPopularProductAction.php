<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    protected $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $allProducts = $this->repository->findAll();
        $mostPopular = collect($allProducts)->sortByDesc(function($product) {
            return($product->getRating());
        })->take(1)->values();
        return new GetMostPopularProductResponse($mostPopular[0]);
    }
}