<?php

declare(strict_types=1);

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product
{
    protected $id;
    protected $name;
    protected $price;
    protected $url;
    protected $rating;

    public function __construct($id, $name, $price, $url, $rating)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->url = $url;
        $this->rating = $rating;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getImageUrl()
    {
        return $this->url;
    }
    public function getRating()
    {
        return $this->rating;
    }
}