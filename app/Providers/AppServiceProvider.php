<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\ProductRepositoryInterface;
use App\Repository\ProductRepository;
use App\Services\ProductGenerator;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ProductRepositoryInterface::class, function () {
            return new ProductRepository(ProductGenerator::generate());
        });
    }

    public function boot()
    {

    }
}
