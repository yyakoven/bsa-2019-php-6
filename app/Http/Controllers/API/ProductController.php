<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Action\Product\GetAllProductsAction;
use App\Repository\ProductRepositoryInterface;
use App\Http\Presenter\ProductArrayPresenter;
use App\Action\Product\GetMostPopularProductAction;
use App\Action\Product\GetCheapestProductsAction;

class ProductController extends Controller
{
    protected $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $action = new GetAllProductsAction($this->repository);
        $products = $action->execute()->getProducts();
        // dd($products);
        return ProductArrayPresenter::presentCollection($products);
    }

    public function popular()
    {
        $action = new GetMostPopularProductAction($this->repository);
        $product = $action->execute()->getProduct();
        return ProductArrayPresenter::present($product);
    }

    public function cheap()
    {
        $action = new GetCheapestProductsAction($this->repository);
        $cheapest = $action->execute()->getProducts();
        // dd($cheapest);
        return ProductArrayPresenter::presentCollection($cheapest);
    }
}
